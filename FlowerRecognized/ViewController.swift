//
//  ViewController.swift
//  FlowerRecognized
//
//  Created by Nguyen Duc Tai on 4/30/18.
//  Copyright © 2018 Nguyen Duc Tai. All rights reserved.
//

import UIKit
import CoreML
import Vision
import SwiftyJSON
import Alamofire
import SDWebImage
class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    let wikipediaURl = "https://en.wikipedia.org/w/api.php"
    
    

    @IBOutlet weak var cameraButton: UIBarButtonItem!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lbl: UILabel!
    var pickedImage: UIImage?
    var imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .camera
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            guard let ciImage = CIImage(image: image) else {
                fatalError("Could not convert image to CIImage")
            }
            
            detect(flowerImage: ciImage)
            
            
           
        }
        imagePicker.dismiss(animated: true, completion: nil)
    }
    func requestInfo(flowerName: String) {
        let parameters : [String:String] = [
            "format" : "json",
            "action" : "query",
            "prop" : "extracts|pageimages",
            "exintro" : "",
            "explaintext" : "",
            "titles" : flowerName,
            "indexpageids" : "",
            "redirects" : "1",
            "pithumbsize" : "500",
            ]
        Alamofire.request(wikipediaURl, method: .get, parameters: parameters).responseJSON { (response) in
            if response.result.isSuccess {
                print("success")
                print(response)
                let json: JSON = JSON(response.result.value!)
                let pageId = json["query"]["pageids"][0].stringValue
                let content = json["query"]["pages"][pageId]["extract"].stringValue
                let flowerURL = json["query"]["pages"][pageId]["thumbnail"]["source"].stringValue
                self.imageView.sd_setImage(with: URL(string: flowerURL))
                self.lbl.text = content
            }
            else {
                print("Fail request")
            }
        }
    }
        func detect(flowerImage: CIImage) {
        guard let model = try? VNCoreMLModel(for: FlowerClassifier().model) else {
            fatalError("Cannot import model")
        }
        
        let request = VNCoreMLRequest(model: model) { (request, error) in
            let classification = request.results?.first as? VNClassificationObservation
            self.requestInfo(flowerName: (classification?.identifier)!)
            
            self.navigationItem.title = classification?.identifier.capitalized
        }
        
        let handler = VNImageRequestHandler(ciImage: flowerImage)
        
        do {
            try handler.perform([request])
        } catch {
            print(error)
        }
    }
    @IBAction func cameraTapped(_ sender: Any) {
        present(imagePicker, animated: true, completion: nil)
    }
    
    
}

